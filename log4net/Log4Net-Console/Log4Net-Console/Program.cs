﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using System.Reflection;

namespace Log4Net_Console
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            Console.WriteLine("==== Testing ====");
            log.Debug("This is Debug only test");
            log.Info("This is Info test.");
            Console.ReadKey();
        }
    }
}
