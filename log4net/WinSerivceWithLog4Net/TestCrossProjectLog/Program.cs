﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using System.Reflection;
using System.IO;

namespace TestCrossProjectLog
{
    public class Program
    {
        private static readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly string onStartFileName = "on_start.txt";
        private static readonly string onStopFileName = "on_stop.txt";

        static void Main(string[] args)
        {
            
        }

        public void Main()
        {
            //Console.WriteLine("=== This is cross project ===");
            logger.Info("This is project 2 info.");
            logger.Debug("This is project 2 debug.");
            //Console.ReadKey();
        }

        public void OnStart()
        {
            logger.Info("Inside the OnStart function.");
            try
            {
                if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + onStartFileName))
                {
                    File.Delete(AppDomain.CurrentDomain.BaseDirectory + onStartFileName);
                }
                File.Create(AppDomain.CurrentDomain.BaseDirectory + onStartFileName);
            }
            catch (IOException ex)
            {
                logger.Error("This is the start ERROR log - " + ex.ToString());
            }
        }

        public void onStop()
        {
            logger.Info("Inside the OnStop function.");
            try
            {
                if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + onStopFileName))
                {
                    File.Delete(AppDomain.CurrentDomain.BaseDirectory + onStopFileName);
                }
                File.Create(AppDomain.CurrentDomain.BaseDirectory + onStopFileName);
            }
            catch (IOException ex)
            {
                logger.Error("This is the stop ERROR log - " + ex.ToString());
            }
        }
    }
}
