﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace WinSerivceWithLog4Net
{
    public partial class MyService : ServiceBase
    {

        public MyService()
        {
            InitializeComponent();
        }

        public void OnStart()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            TestCrossProjectLog.Program program = new TestCrossProjectLog.Program();
            program.OnStart();
        }

        protected override void OnStop()
        {
            TestCrossProjectLog.Program program = new TestCrossProjectLog.Program();
            program.onStop();
        }
    }
}
